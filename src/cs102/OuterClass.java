package cs102;

class ClassOne {
    void method() {
        System.out.println("1");
    }
}

public class OuterClass {
    public void methodOut() {
        ClassOne classOne = new ClassOne() {
            void method() {
                System.out.println("2");
            }
        };
        classOne.method();
    }
}
